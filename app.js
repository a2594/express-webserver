import express from "express";
import morgan from "morgan";
import path from "path";
const app = express();

//Middleware de aplicación
//Atender cualquier tipo de petición que llegue al servidor (GET, POST, PUT, PATCH, DELETE)

//Middleware | atender peticiones | function | dar respuesta al cliente

//Request y Response son objetos

app.use(morgan("dev"));
app.use(express.static("public"));

//Atender peticiones a través del método GET
app.get("/", (request, response ) => {
    const homePage = path.resolve("./public", "index-1.html");
    response.sendFile(homePage);
});

app.get("/nosotros", (request, response ) => {
    response.send("Página acerca de nosotros");
});

export default app;
